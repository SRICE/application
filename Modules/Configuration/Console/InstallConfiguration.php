<?php

namespace Modules\Configuration\Console;

use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallConfiguration extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'install:module-configuration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(setting("module.configuration.active") == 0){
            $module = Module::find("Configuration");
            $module->enable();
            setting(["module.configuration.active" => 1])->save();
            $this->info("Le module de configuration à été activé");
            return null;
        }else{
            return null;
        }
    }

}
