<head>
    <base href="../../">
    <meta charset="utf-8"/>
    <title>{{ env("APP_NAME") }}</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
    @yield("style")

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="/assets/media/logos/favicon.ico"/>
</head>
