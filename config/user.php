<?php
/**
 * Configuration Module User File
 * php version 7.2.10
 *
 * @category Module
 * @package  Sriceuser
 * @author   Srice <contact@srice.eu>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     GIT:<git@gitlab.com:srice-module/user-srice-module.git>
 */
return [
    'name' => 'User',
];
